//
//  GraphicsViewController.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import UIKit

class GraphicsViewController: UIViewController {
    // MARK: IBOutlets
    @IBOutlet weak var contentGraphic: UITableView!
    
    // MARK: Propierties
    var vieModel = GraphicViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configurationTableView()
        self.bind()
    }
    
    func configurationTableView() {
        contentGraphic.delegate = self
        contentGraphic.dataSource = self
        contentGraphic.register(UINib(nibName: "GraphicDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "graphicss")
        self.vieModel.getDataGraphic()
    }
    
    func bind() {
        vieModel.refreshData = { [weak self] () in
            DispatchQueue.main.async {
                self?.contentGraphic.reloadData()
            }
        }
    }
}

extension GraphicsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("---->>>>>>>>")
        print(vieModel.dataArray.count)
        return vieModel.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "graphicss", for: indexPath) as! GraphicDescriptionTableViewCell
        let questionObject = vieModel.dataArray[indexPath.row]
        cell.getData(question: questionObject)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(indexPath.row * 350)
    }
    
}
