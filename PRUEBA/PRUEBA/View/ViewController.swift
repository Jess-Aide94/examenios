//
//  ViewController.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: Properties
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(NameTableViewCell.nib(), forCellReuseIdentifier: NameTableViewCell.identifier)
        table.register(PhotoTableViewCell.nib(), forCellReuseIdentifier: PhotoTableViewCell.identifier)
        table.register(GraphicsTableViewCell.nib(), forCellReuseIdentifier: GraphicsTableViewCell.identifier)
        return table
    }()
    var index = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.red
//        self.configurationTableView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    func configurationTableView() {
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "camera" {
            let destinationVc = segue.destination as! CameraViewController
        } else if segue.identifier == "graphics" {
            let destinationVC = segue.destination as! GraphicsViewController
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 1:
            print("Celda numero uno")
            let cell = tableView.dequeueReusableCell(withIdentifier: PhotoTableViewCell.identifier,
                                                     for: indexPath) as! PhotoTableViewCell
            return cell
        case 2:
            print("Celda numero dos")
            let cell = tableView.dequeueReusableCell(withIdentifier: GraphicsTableViewCell.identifier, for: indexPath) as! GraphicsTableViewCell
            return cell
        default:
            print("Celda numero tres")
            let cell = tableView.dequeueReusableCell(withIdentifier: NameTableViewCell.identifier,
                                                     for: indexPath) as! NameTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        index = indexPath.row
        switch indexPath.row {
        case 1:
            performSegue(withIdentifier: "camera", sender: self)
        case 2:
            performSegue(withIdentifier: "graphics", sender: self)
        default:
            break
        }
        return indexPath
    }
}

