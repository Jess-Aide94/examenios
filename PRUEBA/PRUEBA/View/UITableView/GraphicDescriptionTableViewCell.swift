//
//  GraphicDescriptionTableViewCell.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import UIKit
import Charts

class GraphicDescriptionTableViewCell: UITableViewCell {
    // MARK: IBOutlets
    @IBOutlet weak var questionsLabel: UILabel!
    @IBOutlet weak var graphicView: PieChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getData(question: QuestionsModel) {
        self.questionsLabel.text = question.text
    }
    
}
