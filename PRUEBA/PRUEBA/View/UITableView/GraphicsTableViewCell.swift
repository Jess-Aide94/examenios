//
//  GraphicsTableViewCell.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import UIKit

class GraphicsTableViewCell: UITableViewCell {
    
    // MARK: Propiedades
    static let identifier = "graphics"
    static func nib() -> UINib {
        return UINib(nibName: "GraphicsTableViewCell", bundle: nil )
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
