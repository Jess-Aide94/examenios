//
//  NameTableViewCell.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import UIKit

class NameTableViewCell: UITableViewCell {

    @IBOutlet weak var nameText: UITextField!
    
    // MARK: Propiedades
    static let identifier = "name"
    static func nib() -> UINib {
        return UINib(nibName: "NameTableViewCell", bundle: nil )
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
