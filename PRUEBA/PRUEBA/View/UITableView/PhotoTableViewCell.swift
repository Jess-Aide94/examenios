//
//  PhotoTableViewCell.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {
    //MARK: Propiedades
    static let identifier = "photo"
    static func nib() -> UINib {
        return UINib(nibName: "PhotoTableViewCell", bundle: nil )
    }
    var imagePicker: UIImagePickerController!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}


