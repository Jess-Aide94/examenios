//
//  GraphicsviewModel.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import Foundation

class GraphicViewModel {
    var refreshData = { () -> () in }
    var dataArray: [QuestionsModel] = [] {
        didSet {
            self.refreshData()
        }
    }
    
    var dataColor: [ColorModel] = [] {
        didSet {
            self.refreshData()
        }
    }
    
    func getDataGraphic() {
        let url = URL(string: Constants.url)
        var request = URLRequest(url: url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                  let response = response,
                  error == nil else {
                print("Error in Response")
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                let colors = json!["colors"] as! [String]
                let questions = json!["questions"] as! [[String:Any]]
                self.addDataModel(questionsData: questions)
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func addDataModel(questionsData: [[String:Any]]) -> [QuestionsModel]{
        var arrayQuestionModel: [QuestionsModel] = []
        for question in questionsData {
            let charDatas = question["chartData"] as! [[String:Any]]
            for chart in charDatas {
                let arrayQuestion = QuestionsModel(chartData: PorcentageModel(percetnage: chart["percetnage"] as! Int,
                                                                              text: (chart["text"] as? String)!),
                                                   text: question["text"] as! String,
                                                   total: question["total"] as! Int)
                            arrayQuestionModel.append(arrayQuestion)
            }
        }
        return arrayQuestionModel
    }
    

}


