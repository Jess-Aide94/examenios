//
//  JsonModel.swift
//  PRUEBA
//
//  Created by Jessica Vera Perez on 08/07/21.
//

import Foundation

struct ColorModel {
    let color: [String]
}

struct QuestionsModel {
    let chartData: PorcentageModel
    let text: String
    let total: Int
}

struct PorcentageModel {
    let percetnage: Int
    let text: String
}


